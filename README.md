# l i b w t a r
An implementation of a custom file bundling algorithm inspired by the [ustar]
(https://en.wikipedia.org/wiki/Tar_(computing)) algorithm.

# C O M P I L I N G
You can either perform `make` or `dub` in the project root to build.
The makefile supports the following primary rules:
* all - build every rule
* release - (default rule) builds the library in release mode with optimizations
* debug - builds a debugging build of the library, containing debugging
	symbols and with debug code enabled
* docs - build inline documentation
* test - builds unittests with debugging symbols
* clean - deletes generated build files
* package - constructs bundles for use in providing pre-built packages
* install - installs the library for system-wide use, supports DESTDIR setting
* uninstall - uninstalls the library, supports DESTDIR setting because why not

You can alternatively suffix `release`, `debug`, and `test` with `32` or `64` to
restrict builds to those architectures.

For `dub`, see dub's documentation on usage. Note that this is only minorly
supported. For serious use, default to the makefile.
