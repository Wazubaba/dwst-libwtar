module dwst.wtar;

import std.stdio;
import std.file;
import std.string;
import std.path;
import std.conv;
import std.algorithm.mutation: stripLeft;
import std.base64;
import std.utf;

string MAGIC = "WSTAR00\0\0";

// Mapping to the output of stat for file-types
enum FileType
{ // Of these, I know how to deal with 2 :/
	regular = octal!100000, // This
	socket = octal!140000,
	symlink = octal!120000,
	chardevice = octal!20000,
	blkdevice = octal!60000,
	directory = octal!40000, // And this...
	fifo = octal!10000,
	text = octal!150000 // and this, because this is mine :D
};

class InflationError: Exception
{
	@safe nothrow this(string msg)
	{
		super(msg);
	}
}

class ScanError: Exception
{
	@safe nothrow this(string msg)
	{
		super(msg);
	}
}

class ImportError: Exception
{
	@safe nothrow this(string msg)
	{
		super(msg);
	}
}


/++
	Contains data about one node in an archive.
++/
struct WSTarNode
{
	string path;
	FileType type;
	size_t datalen;
	char[] data;

	const string toString()
	{
		string rhs;
		switch (type) with (FileType)
		{
			case text:
				rhs = "%s\0%d\0%s\0%s\0\0".format(this.path, this.type, this.datalen, this.data); break;
			default:
				rhs = "%s\0%d\0%s\0%s\0\0".format(this.path, this.type, this.datalen, Base64.decode(this.data)); break;
		}

		return rhs;
	}

	/++
		Inflates data into actual directories or files.
	++/
	void Inflate(string destdir = "")
	{
		if (!destdir.exists) throw new InflationError("destdir does not exist");
		if (!destdir.isDir) throw new InflationError("destdir is not a directory");

		switch(type) with (FileType)
		{
			case regular: std.file.write(buildPath(destdir, path), Base64.decode(data)); return;
			case text: std.file.write(buildPath(destdir, path), data); return;
			case directory: mkdirRecurse(buildPath(destdir, path)); return;
			default: return;
		}
	}

	static WSTarNode ScanInit(string path, size_t pathSplitCrop = 0)
	{
		WSTarNode rhs;
		rhs.Scan(path, pathSplitCrop);
		return rhs;
	}

	version(Posix) void Scan(string path, size_t pathSplitCrop = 0)
	{
		if (!path.exists) throw new ScanError("Path does not exist");

		auto entry = DirEntry(path);
		auto entryStats = entry.statBuf();

		this.path = entry.name.StripBasePath(pathSplitCrop);

		if (entryStats.st_mode & FileType.directory)
			this.type = FileType.directory;
		else
		if (entryStats.st_mode & FileType.regular)
		{
				this.type = FileType.regular;
				this.datalen = to!size_t(entryStats.st_size);
				ubyte[] fileData = cast(ubyte[]) read(entry.name);
				// Why did they use an exception instead of a bool!?
				try
				{
					fileData.assumeUTF.validate;
					this.type = FileType.text;
				}
				catch (UTFException e){} // I truely cannot fathom why they did this
				if (this.type == FileType.text) // is this text?
					this.data = fileData.assumeUTF;
				else
					this.data = Base64.encode(fileData);
		}
	}

	version(Windows) void Scan(string path, size_t pathSplitCrop = 0)
	{
		if (!path.exists) throw new ScanError("Path does not exist");

		this.path = path.StripBasePath(pathSplitCrop);

		if (path.isFile)
			this.type = FileType.directory;
		else
		{
			this.type = FileType.regular;
			this.datalen = to!size_t(path.getSize());
			writeln("got size!");
			ubyte[] fileData = cast(ubyte[]) read(path);

			// Why did they use an exception instead of a bool!?
			try
			{
				fileData.assumeUTF.validate;
				this.type = FileType.text;
			}
			catch (UTFException e){} // I truely cannot fathom why they did this
				if (this.type == FileType.text) // is this text?
					this.data = fileData.assumeUTF;
				else
					this.data = Base64.encode(fileData);
		}
	}
}

class ArchivalData
{
	WSTarNode[] data;

	this(){}

	/++
		Initializes archive with data from this path
	++/
	this(string path)
	{
		ScanTargetDirectory(path);
	}

	/++
		Inflate the archive into the file system.
		Takes:
			string destdir = "" - optional path to export into
	++/
	void Inflate(string destdir = "")
	{ // Take two passes to ensure all directories exist
		foreach (hdr; data)
			if (hdr.type == FileType.directory)
				hdr.Inflate(destdir);
		foreach (hdr; data)
			if (hdr.type != FileType.directory)
				hdr.Inflate(destdir);
	}

	override string toString()
	{
		string rhs;

		rhs ~= MAGIC;
		rhs ~= "\0%s\0".format(data.length);
		foreach (hdr; data)
			rhs ~= "%s\0%d\0%s\0%s\0\0".format(hdr.path, hdr.type, hdr.datalen, Base64.decode(hdr.data));

		return rhs;
	}

	/++
		Expand the data into a string for further processing.
		Returns:
			string rhs - data serialized into a string
	++/
	string Serialize()
	{
		string rhs;

		rhs ~= MAGIC;
		rhs ~= "\0%s\0".format(data.length);
		foreach (hdr; data)
			rhs ~= "%s\0%d\0%s\0%s\0\0".format(hdr.path, hdr.type, hdr.datalen, hdr.data);

		return rhs;
	}

	/++
		Imports an existing wtar archive.
		Takes:
			string path - path to wtar archive
		Throws:
			ImportError - if archive does not exist
			InflationError - if archive is damaged or invalid
	++/
	void Import(string path)
	{
		if (!path.exists)
			throw new ImportError("Directory does not exist");

		if (!path.isFile)
			throw new ImportError("Is not a file");

		string fileData = cast(string) std.file.read(path);

		if (fileData[0..9] != MAGIC)
			throw new InflationError("Not a valid wstar archive stream");

		// Strip MAGIC prefix off to simplify parsing
		fileData = fileData[10..$];

		auto hdrlen = to!size_t(fileData.split('\0')[0]);

		// Strip hdrlen data to simplify parsing
		fileData = fileData[fileData.split('\0')[0].length..$];

		size_t index = 0;

		for (size_t i = 0; i < hdrlen; i++)
		{
			WSTarNode leaf;
			// Each tick we need to take care to strip a possible record seperator
			fileData = fileData.stripLeft('\0');

			try
			{
				leaf.path = fileData[index..fileData.indexOf('\0', index)];
				index = fileData.indexOf('\0', index) + 1;

				int typestore = to!int(fileData[index..fileData.indexOf('\0', index)]);
				index = fileData.indexOf('\0', index) + 1;

				if (typestore == FileType.directory)
					leaf.type = FileType.directory;
				else
				if (typestore == FileType.regular)
					leaf.type = FileType.regular;
				else
				if (typestore == FileType.text)
					leaf.type = FileType.text;

				leaf.datalen = to!size_t(fileData[index..fileData.indexOf('\0', index)]);
				index = fileData.indexOf('\0', index) + 1;

				if (leaf.datalen > 0)
					leaf.data = cast(char[]) fileData[index..fileData.indexOf('\0', index)];

				index = fileData.indexOf('\0', index) + 2; // Skip end of record as well

				data ~= leaf;
			}
			catch (ConvException e)
			{
				writeln(e.msg);
				writefln("***\n%s\n%s\n%s\n%s\n***", leaf.path, leaf.type, leaf.datalen, leaf.data);
			}
		}
	}

	/++
		Import the serialized data of an archive.
		Takes:
			string fileData - serialized data from an archive
		Throws:
			InflationError - if archive is damaged or invalid
	++/
	void Inject(string fileData)
	{
		if (fileData[0..9] != MAGIC)
			throw new InflationError("Not a valid wstar archive stream");

		// Strip MAGIC prefix off to simplify parsing
		fileData = fileData[10..$];

		auto hdrlen = to!size_t(fileData.split('\0')[0]);

		// Strip hdrlen data to simplify parsing
		fileData = fileData[fileData.split('\0')[0].length..$];

		size_t index = 0;

		for (size_t i = 0; i < hdrlen; i++)
		{
			WSTarNode leaf;
			// Each tick we need to take care to strip a possible record seperator
			fileData = fileData.stripLeft('\0');

			try
			{
				leaf.path = fileData[index..fileData.indexOf('\0', index)];
				index = fileData.indexOf('\0', index) + 1;

				int typestore = to!int(fileData[index..fileData.indexOf('\0', index)]);
				index = fileData.indexOf('\0', index) + 1;

				if (typestore == FileType.directory)
					leaf.type = FileType.directory;
				else
				if (typestore == FileType.regular)
					leaf.type = FileType.regular;
				else
				if (typestore == FileType.text)
					leaf.type = FileType.text;

				leaf.datalen = to!size_t(fileData[index..fileData.indexOf('\0', index)]);
				index = fileData.indexOf('\0', index) + 1;

				if (leaf.datalen > 0)
					leaf.data = cast(char[]) fileData[index..fileData.indexOf('\0', index)];

				index = fileData.indexOf('\0', index) + 2; // Skip end of record as well

				data ~= leaf;
			}
			catch (ConvException e)
			{
				writeln(e.msg);
				writefln("***\n%s\n%s\n%s\n%s\n***", leaf.path, leaf.type, leaf.datalen, leaf.data);
			}
		}
	}
	/*
	void Import(string path)
	{
		if (!path.exists)
			throw new Exception("Directory does not exist");

		if (!path.isFile)
			throw new Exception("Is not a file");

		ubyte[] fileData = cast(ubyte[]) std.file.read(path);
		auto stuff = fileData.assumeUTF();

		assert (cast(ubyte[]) stuff == fileData);

		if (stuff[0..9] != MAGIC)
			throw new Exception("Invalid wtar archive stream");

		// Strip MAGIC prefix
		stuff = stuff[10..$];

		// Strip hdrlen data
		auto hdrlen = to!size_t(data.split('\0')[0]);
		stuff = stuff[stuff.split('\0')[0].length..$];

		size_t index = 0;

		for (size_t i = 0; i < hdrlen; i++)
		{
			WSTarNode leaf;
			stuff = stuff.stripLeft('\0');

			try
			{
				hdr.path = stuff[index..stuff.indexOf('\0', index)];
				index = stuff.indexOf('\0', index) + 1;

				int typeStore = to!int(stuff[index..stuff.indexOf('\0', index)]);
				index = stuff.indexOf('\0', index) + 1;

				if (typestore & FileType.directory)
					hdr.type = FileType.directory;
				else
				if (typestore & FileType.regular)
					hdr.type = FileType.regular;
				else
				if (typestore & FileType.text)
					hdr.type = FileType.text;

				hdr.datalen = to!size_t(stuff[index..data.indexOf('\0', index)]);
				index = stuff.indexOf('\0', index) + 1;

				if (hdr.datalen > 0)
					hdr.data = cast(char[])
			}
		}




/*

					try
					{
						hdr.path = data[index..data.indexOf('\0', index)];
						index = data.indexOf('\0', index) + 1;

						int typestore = to!int(data[index..data.indexOf('\0', index)]);
						index = data.indexOf('\0', index) + 1;

						if (typestore & FileType.directory)
							hdr.type = FileType.directory;
						else
						if (typestore & FileType.regular)
							hdr.type = FileType.regular;

						hdr.datalen = to!size_t(data[index..data.indexOf('\0', index)]);
						index = data.indexOf('\0', index) + 1;

						if (hdr.datalen > 0)
							hdr.data = cast(ubyte[]) data[index..data.indexOf('\0', index)];

						index = data.indexOf('\0', index) + 2; // Skip end of record as well

						archive ~= hdr;
					}
					catch (ConvException e)
					{
						writeln(e.msg);
						writefln("***\n%s\n%s\n%s\n%s\n***", hdr.path, hdr.type, hdr.datalen, hdr.data);
					}
				}

				return archive;
			}
	}
*/
	/++
		Scan a target directory and populate the archive with its contents.
		Takes:
			string path - path to the target directory
		Throws:
			ScanError - If target directory does not exist or is otherwise invalid
	++/
	void ScanTargetDirectory(string path)
	{
		if (!path.exists)
			throw new ScanError("Directory does not exist");

		if (!path.isDir)
			throw new ScanError("Is not a directory");

		WSTarNode baseDir;

		baseDir.Scan(path);
		baseDir.path = baseDir.path.baseName;

		data ~= baseDir;

		string[] s;
		foreach (subpath; path.pathSplitter())
			s ~= subpath;

		size_t pathCropRange = s.length - 1;

		foreach (DirEntry subEntry; dirEntries(path, SpanMode.depth))
		{
			WSTarNode leaf = WSTarNode.ScanInit(subEntry.name, pathCropRange);
			data ~= leaf;
		}
	}
}
/++
	Strip the base path from a given path, up to index n.
	Takes:
		string path - path to alter
		size_t index - index to cull up to
	Returns:
		string s - Altered path
++/
string StripBasePath(string path, size_t index)
{
	string[] s;
	foreach (subpath; path.pathSplitter())
		s ~= subpath;

	s = s[index..$];
	string rhs;
	foreach (subpath; s)
		rhs ~= subpath ~ dirSeparator;
	return rhs[0..$-1]; // Strip off the trailing seperator
}

unittest
{
	auto archive = new ArchivalData("testdir");
//	writeln(archive);
	std.file.write("xdirtest.wtar", archive.Serialize());

	archive.destroy();

	auto arc = new ArchivalData();

	arc.Import("xdirtest.wtar");

//	writeln(arc.Serialize());

	arc.Inflate("outTEST");

	//assert(archive.data.length == 1);
}
